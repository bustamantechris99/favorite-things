Welcome to the first Git exercise!
Completing these prompts will help you get more comfortable with
Git.

1. What is your favorite color?
Navy Blue

***
  Remember to:
    - add
    - commit
    - push
  your answer before answering the next question!
***

2. What is your favorite food?
Sushi Sake


3. Who is your favorite fictional character?
Iron Man


4. What is your favorite animal?
Monkeys


5. What is your favorite programming language? (Hint: You can always say Python!!)
Stuck between Javascript and Python. Python is easier, Javacript seems a little more fun to me.